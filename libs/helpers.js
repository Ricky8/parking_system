convert = (time_in, time_out) => {
    const timeIn = new Date(time_in)
    const timeOut = new Date(time_out)

    let total = 0

    let y = timeOut.getFullYear() - timeIn.getFullYear()
    y = y * (365 * 24 * 60 * 60)
    total += y

    let m = timeOut.getMonth() - timeIn.getMonth()
    m = m * (30 * 24 * 60 * 60)
    total += m

    let d = timeOut.getDate() - timeIn.getDate()
    d = d * (24 * 60 * 60)
    total += d

    let h = timeOut.getHours() - timeIn.getHours()
    h = h * (60 * 60)
    total += h

    let mt = timeOut.getMinutes() - timeIn.getMinutes()
    mt = mt * (60)
    total += mt

    let s = timeOut.getSeconds() - timeIn.getSeconds()
    total += s

    if (total <= 0) {
        return 0
    }

    return total
}

cost = (time_in, time_out, days_rate, hours_rate) => {
    const time_diff = convert(time_in, time_out)
    const one_day = (24 * 60 * 60)

    let total = 0

    if (time_diff >= one_day) {
        const time_diff_in_hours = time_diff / (60 * 60)
        if (time_diff_in_hours >= 24) {
            const days_count = Math.floor(time_diff_in_hours / 24)
            const hours_count = time_diff_in_hours % 24

            const days_cost = days_count * days_rate
            const hours_cost = hours_count * hours_rate

            total = days_cost + hours_cost
        } else {
            const hours_cost = time_diff_in_hours * hours_rate
            total = hours_cost
        }
    } else {
        const time_diff_in_minutes = Math.floor(time_diff / 60)
        let time_diff_rest = time_diff % 60

        if (time_diff_in_minutes >= 60) {
            const hours_count = Math.floor(time_diff_in_minutes / 60)
            const time_diff_in_minutes_rest = time_diff_in_minutes % 60

            const time_diff_in_minutes_rest_to_second = time_diff_in_minutes_rest * 60
            time_diff_rest += time_diff_in_minutes_rest_to_second

            total = hours_count * hours_rate
            if (time_diff_rest >= 61) {
                total += total
            }
        } else {
            const time_diff_in_second = time_diff_in_minutes * 60
            time_diff_rest += time_diff_in_second
            if (time_diff_rest >= 61) {
                total += hours_rate
            }
        }
    }
    return total
}

module.exports = {
    convert,
    cost
}