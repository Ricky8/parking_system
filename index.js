const logging = require('./libs/logging')

const express = require('express')
const app = express()
const cors = require('cors')
const compression = require('compression')
const bodyParser = require('body-parser')

const swaggerUi = require('swagger-ui-express');
const invoiceDocs = require('./documentations/invoiceDocument.json');

require('dotenv').config()

const port = process.env.APP_PORT

// routed
const invoice = require('./routes/invoice.js')

logging.init({
    path: process.env.LOG_DIR,
    level: process.env.LOG_LEVEL
})

app.use(compression())
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/api/v1/invoice', invoice)
app.use('/invoice-docs', swaggerUi.serve, swaggerUi.setup(invoiceDocs));

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send({ error: err.message });
});

app.use(function(req, res) {
    res.status(404);
    res.send({ error: "Sorry, page not found" })
});

app.listen(port)
logging.info('[app] STARTED on ' + port)