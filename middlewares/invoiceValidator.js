const { check, query, validationResult } = require('express-validator')
const db = require("../db/models")

const createValidate = [
    check('price_id').custom(value => {
        return db.price.findByPk(value).then(result => {
            if (result === null) {
                return Promise.reject("no vehicle category found")
            }
        })
    }),
    check('time_in').isISO8601().toDate(),
    check('time_out').isISO8601().toDate(),
    (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).send({ errors: errors.array() });
        }

        next()
    }
]

const listValidate = [
    query('price_id').optional({ checkFalsy: false }).custom(value => {
        return db.price.findByPk(value).then(result => {
            if (result === null) {
                return Promise.reject("no vehicle category found")
            }
        })
    }),
    query('date_from').optional({ checkFalsy: false }).isISO8601().toDate(),
    query('date_to').optional({ checkFalsy: false }).isISO8601().toDate(),
    query('cost_from').optional({ checkFalsy: false }).isNumeric(),
    query('cost_to').optional({ checkFalsy: false }).isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).send({ errors: errors.array() });
        }

        next()
    }
]

module.exports = {
    createValidate,
    listValidate
}