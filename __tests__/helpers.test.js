const { convert, cost } = require('../libs/helpers')

test('convert 1 year to second', () => {
    expect(convert("2015-03-25T12:00:00Z", "2016-03-25T12:00:00Z")).toBe(31536000)
})

test('convert 1 month to second', () => {
    expect(convert("2015-03-25T12:00:00Z", "2015-04-25T12:00:00Z")).toBe(2592000)
})

test('convert 1 date to second', () => {
    expect(convert("2015-03-25T12:00:00Z", "2015-03-26T12:00:00Z")).toBe(86400)
})

test('convert 1 hour to second', () => {
    expect(convert("2015-03-25T12:00:00Z", "2015-03-25T13:00:00Z")).toBe(3600)
})

test('convert 1 minute to second', () => {
    expect(convert("2015-03-25T12:00:00Z", "2015-03-25T12:01:00Z")).toBe(60)
})

test('convert 1 year, 1 month, 1 date, 1 hour, 1 minute to second', () => {
    expect(convert("2015-03-25T12:00:00Z", "2016-04-26T13:01:00Z")).toBe(34218060)
})

test('convert 0 year, 0 month, 0 date, 0 hour, 0 minute to second', () => {
    expect(convert("2015-03-25T12:00:00Z", "2015-03-25T12:00:00Z")).toBe(0)
})

test('if time in is greater than time out', () => {
    expect(convert("2016-03-25T12:00:00Z", "2015-03-25T12:00:00Z")).toBe(0)
})

test('motorcyle in 1 hour, 1 minute, 1 second', () => {
    expect(cost("2016-03-25T01:00:00Z", "2016-03-25T02:01:01Z", 40000, 2000)).toBe(4000)
})

test('motorcyle in 1 hour, 56 second', () => {
    expect(cost("2016-03-25T01:00:00Z", "2016-03-25T02:00:56Z", 40000, 2000)).toBe(2000)
})

test('motorcyle in 1 minute', () => {
    expect(cost("2016-03-25T01:00:00Z", "2016-03-25T01:01:00Z", 40000, 2000)).toBe(0)
})

test('motorcyle in 1 minute 1 second', () => {
    expect(cost("2016-03-25T01:00:00Z", "2016-03-25T01:01:01Z", 40000, 2000)).toBe(2000)
})

test('motorcyle in 1 day', () => {
    expect(cost("2016-03-25T01:00:00Z", "2016-03-26T01:00:00Z", 40000, 2000)).toBe(40000)
})

test('motorcyle in 1 day 1 hour', () => {
    expect(cost("2016-03-25T01:00:00Z", "2016-03-26T02:00:00Z", 40000, 2000)).toBe(42000)
})

test('car in 1 hour, 1 minute, 1 second', () => {
    expect(cost("2016-03-25T01:00:00Z", "2016-03-25T02:01:01Z", 80000, 5000)).toBe(10000)
})

test('car in 1 hour, 56 second', () => {
    expect(cost("2016-03-25T01:00:00Z", "2016-03-25T02:00:56Z", 80000, 5000)).toBe(5000)
})

test('car in 1 minute', () => {
    expect(cost("2016-03-25T01:00:00Z", "2016-03-25T01:01:00Z", 80000, 5000)).toBe(0)
})

test('car in 1 minute 1 second', () => {
    expect(cost("2016-03-25T01:00:00Z", "2016-03-25T01:01:01Z", 80000, 5000)).toBe(5000)
})

test('car in 1 day', () => {
    expect(cost("2016-03-25T01:00:00Z", "2016-03-26T01:00:00Z", 80000, 5000)).toBe(80000)
})

test('car in 1 day 1 hour', () => {
    expect(cost("2016-03-25T01:00:00Z", "2016-03-26T02:00:00Z", 80000, 5000)).toBe(85000)
})