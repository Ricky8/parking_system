# Parking System

## Installation
```bash
npm install

npx sequelize db:migrate

npx sequelize db:seed:all

npm run start
```

## Unit Test
```bash
npm run test
```

## Run Documentation
```bash
http://host:port/invoice-docs
```