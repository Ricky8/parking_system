'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class price extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  price.init({
    name: DataTypes.STRING,
    day_rate: DataTypes.INTEGER,
    hour_rate: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'price',
    underscored: true,
  });
  return price;
};