'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('vehicles', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            price_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: {
                        tableName: 'prices',
                    },
                    key: 'id',
                },
                allowNull: false,
            },
            time_in: {
                type: Sequelize.DATE
            },
            time_out: {
                type: Sequelize.DATE
            },
            cost: {
                type: Sequelize.INTEGER
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('vehicles');
    }
};