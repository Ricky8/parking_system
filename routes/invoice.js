const express = require('express')
const router = express.Router()
const { createValidate, listValidate } = require('../middlewares/invoiceValidator')

const invoiceController = require("../controllers/invoiceControllers")

router.post('/create', createValidate, invoiceController.create)
router.get('/list', listValidate, invoiceController.list)

module.exports = router