const { error } = require('../libs/utils')
const logging = require('../libs/logging')
const db = require("../db/models")
const op = db.Sequelize.Op

const { cost } = require('../libs/helpers')

create = async(req, res, next) => {
    try {
        const { price_id, time_in, time_out } = req.body

        const prices = await db.price.findByPk(price_id)
        if (prices === null) {
            return res.status(404).send({
                "message": "No Vehicle Found"
            })
        }

        logging.debug(`[GET PRICES] ${JSON.stringify(prices)}`)
        const total_cost = cost(time_in, time_out, prices.day_rate, prices.hour_rate)

        const result = await db.vehicle.create({ price_id: price_id, time_in: time_in, time_out: time_out, cost: total_cost })
        logging.debug(`[Vehicle Create] ${JSON.stringify(result)}`)

        return res.status(201).send({
            "message": "Created"
        })
    } catch (e) {
        logging.error(`[Create error]${JSON.stringify(e)}`)
        next(error(400, 'Something went wrong'))
    }
}

list = async(req, res, next) => {
    try {
        const { price_id, date_from, date_to, cost_from, cost_to } = req.query

        let time_in_query = {}

        if (date_from === undefined && date_to === undefined) {
            return res.status(423).send({
                "message": "No date found, send at least one date params"
            })
        } else if (date_from === undefined) {
            time_in_query = {
                [op.lte]: date_to
            }
        } else if (date_to === undefined) {
            time_in_query = {
                [op.gte]: date_from
            }
        } else {
            time_in_query = {
                [op.gte]: date_from,
                [op.lte]: date_to
            }
        }

        let category_in_query = {}

        if (price_id === undefined) {
            category_in_query = {
                [op.gt]: 0
            }
        } else {
            category_in_query = {
                [op.eq]: price_id
            }
        }

        let cost_in_query = {}

        if (cost_from === undefined && cost_to === undefined) {
            return res.status(424).send({
                "message": "No cost found, send at least one cost params"
            })
        } else if (cost_from === undefined) {
            cost_in_query = {
                [op.lte]: cost_to
            }
        } else if (cost_to === undefined) {
            cost_in_query = {
                [op.gte]: cost_from
            }
        } else {
            cost_in_query = {
                [op.gte]: cost_from,
                [op.lte]: cost_to
            }
        }

        const result = await db.vehicle.findAll({
            attributes: ["id", "cost", "price_id", "time_in", "time_out", "created_at", "updated_at"],
            where: {
                price_id: category_in_query,
                time_in: time_in_query,
                cost: cost_in_query
            }
        })
        logging.debug(`[List Result] ${JSON.stringify(result)}`)

        if (result.length <= 0) {
            return res.status(404).send({
                message: "Data not found"
            })
        }

        return res.status(200).send({
            data: result
        })
    } catch (e) {
        logging.error(`[List error]${JSON.stringify(e)}`)
        next(error(400, 'Something went wrong'))
    }
}

module.exports = {
    create,
    list
}